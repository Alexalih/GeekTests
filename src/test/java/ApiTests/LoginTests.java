package ApiTests;

import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;



import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class LoginTests extends AbstractTest {

    @Test
    @DisplayName("Авторизация с валидными данными")
    @Tag("Positive")
    void loginValidTest (){


         JsonPath body = given()
                .contentType("multipart/form-data")
                .multiPart("username", getLogin())
                .multiPart("password", getPass())
                .when()
                .post(getBaseUrl()+getPathLogin())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();

         assertThat(body.getString("username"), equalTo("alexlih"));
         assertThat(body.getString("token"), not(""));


    }
    @ParameterizedTest
    @CsvFileSource (resources = "parametrsLogin.csv")
    @DisplayName("Авторизация с невалидными данными")
    @Tag("Negative")
    void loginInValidTest (String username, String ps){


        JsonPath body = given()
                .contentType("multipart/form-data")
                .multiPart("username", username)
                .multiPart("password", ps)
                .when()
                .post(getBaseUrl()+getPathLogin())
                .then()
                .statusCode(401)
                .extract().body().jsonPath();

        assertThat(body.getString("error"), equalTo("Invalid credentials."));
        assertThat(body.getString("code"), equalTo("401"));

    }


}
