package UITests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.NoSuchElementException;


public class MyBlogTests extends AbstractTest {

    @Test
    @DisplayName("Отображение постов  у пользователя у которого есть посты")
    @Tag("Positive")
    public void postDisplayTest() {
        getLoginPage().login(getLogin(), getPass());
        //  Сортировку нажимаем два раза, потому что в данной реализации чтобы сделать ASC сортировку на сайте нужно нажать 2 раза
        getMainPage().clickSort();
        getMainPage().clickSort();

        try {
            String firstPostImg = getMainPage().getFirstPostImg().getAttribute("src");
            Assertions.assertEquals(firstPostImg, "http://test-stand.gb.ru/files/public/image/08d9fc4b8c7d82a9d710b2e354cb4c0c.jpg");

            String firstPostTitle = getMainPage().getFirstPostTitle().getText();
            Assertions.assertEquals(firstPostTitle, "Первый пост");

            String firstPostDescr = getMainPage().getFirstPostDescription().getText();
            Assertions.assertEquals(firstPostDescr, "пост");
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }


    }

    @Test
    @DisplayName("Отображение заглушки у пользователя у которого нет постов")
    @Tag("Positive")
    public void NoContentPostTest() {
        getLoginPage().login(getEmptyLogin(), getEmptyPass());
        try {
            String textNoContent = getMainPage().getNoContent().getText();
            Assertions.assertEquals(textNoContent, "No items for your filter");
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }

    }


    @Test
    @DisplayName("Переключение на следующую страницу")
    @Tag("Positive")
    public void clickNextPagesTest() throws InterruptedException {
        getLoginPage().login(getLogin(), getPass());

        try {
            String nextPageHref = getMainPage().getNextPage().getAttribute("href");
            assert (nextPageHref.contains("/?page=2"));

            String nextPageText = getMainPage().getNextPage().getText();
            Assertions.assertEquals(nextPageText, "Next Page");

            getMainPage().clickNextPage();
            Thread.sleep(5000);
            Assertions.assertEquals(getDriver().getCurrentUrl(), "https://test-stand.gb.ru/api/posts?sort=createdAt&order=ASC&page=2");
            getMainPage().getLastPost();

        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    @DisplayName("Переключение на предыдущую страницу")
    @Tag("Positive")
    public void clickPrevPagesTest() throws InterruptedException {

        getLoginPage().login(getLogin(), getPass());

        try {
            String previousPageText = getMainPage().getPreviousPage().getText();
            Assertions.assertEquals(previousPageText, "Previous Page");
            String previousPageClass = getMainPage().getPreviousPage().getAttribute("class");
            assert (previousPageClass.contains("disabled"));

            getMainPage().clickNextPage();
            Thread.sleep(5000);
            String prevPageHref = getMainPage().getPreviousPage().getAttribute("href");
            assert (prevPageHref.contains("/?page=1"));

            getMainPage().clickPrevPage();
            Thread.sleep(5000);
            assert (getDriver().getCurrentUrl().contains("/?page=1"));

            } catch (NoSuchElementException e) {
                Assertions.assertTrue(false);
            }
    }


}
