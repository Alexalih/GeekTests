package UITests;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

@Getter
public class LoginPage extends AbstractPage {
    @FindBy(xpath = ".//input[@type=\"text\"]")
    private WebElement loginField;

    @FindBy(xpath = ".//input[@type=\"password\"]")
    private WebElement passField;


    @FindBy(xpath = ".//button[@form=\"login\"]")
    private WebElement loginSubmit;


    @FindBy(xpath = ".//div[@class=\"error-block svelte-uwkxn9\"]/h2")
    private WebElement errorHeader;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(String login, String pass) {
        Actions auth = new Actions(getDriver());
        auth
                .click(this.loginField)
                .sendKeys(login)
                .click(this.passField)
                .sendKeys(pass)
                .click(this.loginSubmit)
                .pause(2000)
                .build()
                .perform();
    }
}
