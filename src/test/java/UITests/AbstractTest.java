package UITests;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public abstract class AbstractTest {
    @Getter
    private static WebDriver driver;
    static Properties myProp = new Properties();
    private static InputStream config;
    @Getter
    private static String loginUrl;
    @Getter
    private static String baseUrl;
    @Getter
    private static String login;
    @Getter
    private static String pass;
    @Getter
    private  static String emptyLogin;
    @Getter
    private  static String emptyPass;
    @Getter
    private static MainPage mainPage;
    @Getter
    private static LoginPage loginPage;

    @BeforeAll
    static void init() throws IOException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("start-maximized");
//        options.addArguments("--headless");

        config = new FileInputStream("./src/main/resources/my.properties");
        myProp.load(config);
        baseUrl= myProp.getProperty("baseUrl");
        loginUrl = myProp.getProperty("loginUrl");
        login=myProp.getProperty("login");
        pass=myProp.getProperty("pass");
        emptyLogin=myProp.getProperty("emptyLogin");
        emptyPass=myProp.getProperty("emptyPass");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        mainPage = new MainPage(getDriver());
        loginPage = new LoginPage(getDriver());

    }

    @AfterAll
    static void close() throws InterruptedException {
        driver.quit();
    }

//    @AfterEach
//    void logout() throws InterruptedException {
//        mainPage.logout();
//    }

    @BeforeEach
    void goTo() {
        Assertions.assertDoesNotThrow(() -> driver.navigate().to(loginUrl), "Страница не доступна");
    }

}
